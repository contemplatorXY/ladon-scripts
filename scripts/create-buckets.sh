# create test data filesystem
# autor: ggaida
#Parameter $1 Pfad, $2 Beginn-Index $3 Ende-Index
# 21.06.2020

for i in `seq -w $2 $3`;
do
  mkdir ./$1/anankebucket$i;
  echo ./$1/anankebucket$i;
  for j in `seq -w $2 $3`;
  do
    printf "Ananke-Test-Data;anankebucket$i;`date`;anankebucket$i/ananketextfile$j, size : 18kB\n" > ./temp.txt;
    cat ./temp.txt ./testdata.txt > ./$1/anankebucket$i/ananketextfile$j;
  done
done
