#!/bin/sh
# kill zob sessions
# author ggi
# date 03.09.2021
index="7050360,8943180,7402500,8056400,8423150,8602980,9077500,8946080,3413520,4560560,7626790,9238380"
echo "we are in $1"
(IFS=',';
   for inc in $index;
     do
       echo  killing "$inc";
       curl -X DELETE https://zob.$1.k8s.dvag.net/resources/session/users/$inc -H "accept: */*";
       echo "finished";
       done
)
echo "ZOB-Sessions folgender VB wurden gelöscht: $index" | mail -s "ZOB-Sessions killed" gerhard.gaida.extern@dvag.com