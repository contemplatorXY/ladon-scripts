#!/bin/sh
# ladon-cluster deployment
# author ggi
# date 12.03.2022
index="5,6,8,9"

(IFS=',';
   for inc in $index;
     do
       echo  deploing "lad$inc";
       ./start-ladon-remote.sh "lad$inc";
       echo "finished";
       done
)
echo "done"