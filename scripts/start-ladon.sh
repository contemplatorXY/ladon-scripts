#service ladon startscript
#autor: ggi
#02.02.2020
dir=/srv/ladon
echo "starting ladon ... working directory is " $dir
java  -Dcom.sun.management.jmxremote\
 -Djava.rmi.server.hostname=localhost\
 -Dcom.sun.management.jmxremote.port=7199\
 -Dcom.sun.management.jmxremote.rmi.port=7199\
 -Dcom.sun.management.jmxremote.authenticate=false\
 -Dcom.sun.management.jmxremote.ssl=false\
 -Djava.io.tmpdir=/tmp\
 -XX:ErrorFile=./logs/hs_err_ladon_pid%%p.log\
 -XX:+HeapDumpOnOutOfMemoryError\
 -XX:HeapDumpPath=./logs\
 -Dfile.encoding=ISO-8859-1\
 -Dsun.jnu.encoding=ISO-8859-1\
 -Duser.country=US\
 -Duser.language=en\
 -Dserver.port=8080  -Dladon.db.user=ladon -Dladon.home=../ladon_data -jar ladon.jar  &
echo $! > ./service.pid
echo "Ladon PID " $!