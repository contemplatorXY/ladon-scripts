#!/bin/sh
# ladon-cluster deployment
# author ggi
# date 05.05.2020
index="5,6,8,9"

ladonorg=https://ladon.org/downloads/
ladonfile=ladon_de_
version=$1
wget $ladonorg$ladonfile$version.jar
(IFS=',';
   for inc in $index;
     do
       echo  deploing "lad$inc";
       scp -i.ssh/id_rsa $ladonfile$version.jar supervisor@lad$inc:/srv/ladon
       ./deploy-ladon.sh $1 "lad$inc";
       echo "finished";
       done
)
rm $ladonfile$version.jar
if [ "$#" -eq 2 ] ; then
		echo "Ladon-Deployment $1 \nvielen Dank an alle Beteiligten!\nITOPEA.Supervisor wuenscht eine gute Heimreise" | mail -s "Ladon-Deployment Version  $1 in Itopea-Lab beendet" $2
fi

