#ladon deployment
# author ggi
# date 12.03.2022
#$1 = Server ip

serverip=$1

if [ "$#" -ne 1 ] ; then
		echo "required parameter:  server IP = $serverip" >&2
			exit 1
fi
echo stoping ladon.service on $serverip ...
ssh -t  -i.ssh/id_rsa supervisor@$serverip 'sudo systemctl stop ladon.service'
exit
EOF

