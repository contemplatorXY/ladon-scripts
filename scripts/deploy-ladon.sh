#ladon deployment
# author ggi
# date 04.05.2020
# params $1 = file to deploy, $2 = Server ip


ladonorg=https://ladon.org/downloads/
ladonfile=ladon_de_
version=$1
serverip=$2

if [ "$#" -ne 2 ] ; then
		echo "required parameter:  version = $version, server IP = $serverip" >&2
			exit 1
fi
echo deploying $ladonfile$version.jar to $serverip
echo stoping ladon.service ...
ssh -t  -i.ssh/id_rsa supervisor@$serverip 'sudo systemctl stop ladon.service'
ssh -tt -i./.ssh/id_rsa supervisor@$serverip << EOF
cd /srv/ladon
./shutdown.sh
mv $ladonfile$version.jar ladon.jar
echo $version > version.txt
exit
EOF
scp -i.ssh/id_rsa ./ladon-scripts/scripts/start-ladon.sh ./ladon-scripts/scripts/shutdown.sh ./ladon-scripts/scripts/restart.sh ./ladon-scripts/scripts/ladon.service supervisor@$serverip:/srv/ladon/
echo starting ladon.service ...
ssh -t  -i.ssh/id_rsa supervisor@$serverip 'sudo systemctl restart ladon.service'
