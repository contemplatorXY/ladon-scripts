#ladon operating
# author ggi
# date 11.06.2020
# params $1 = serverip or dns name, $2 = repair opptions (p.e full)


serverip=$1
options=$2


if [ "$#" -eq 0 ] ; then
		echo "required parameter: server IP = $serverip" >&2
			exit 1
fi
echo "`date` repairing cassandra on $serverip "

ssh -tt -i./.ssh/id_rsa supervisor@$serverip << EOF
cd /srv/ladon/apache-cassandra-3.11.6/bin
./nodetool repair --$options
rm result.txt
./nodetool info > result.txt
exit
EOF
if [ "$?" -eq 0 ] ; then
  mv result.txt result.last
  scp -i.ssh/id_rsa supervisor@$serverip:/srv/ladon/apache-cassandra-3.11.6/bin/result.txt .
  if ["$?" -eq 0 ] ; then
     echo "`date` end of repair"
     mailbody=`cat result.txt`
     printf "Ladon operating \nrepair $options \n$mailbody"  | mail -s "Operating cronjob: Ladon repair on $1 in Itopea-Lab beendet" info@itopea.de
   else
	   printf "Ladon operating job exit code $? das Resultat des repairs konnte nicht gefunden werden "  | mail -s "Operating cronjob: Ladon repair on $1 in Itopea-Lab scheiterte" info@itopea.de
  fi
else
	printf "Ladon operating job exit code $? ssh connect auf $serverip "  | mail -s "Operating cronjob: Ladon repair on $1 in Itopea-Lab scheiterte" info@itopea.de
fi