@echo off
setlocal
rem Batch zur Entschluesselung und Entzippung von DEUBA Dateien (dvag_range Dateien)
rem vom WinSFTP (deuba/prod) auf WinSFTP (deuba/prod)
rem Daten kommen unregelmaessig rein, dvag_range*.zip.gpg und sind verschlüsselt
rem Ansprechpartner Susanne Walb
rem
rem 15.06.2021 PKU
set log=\\ucm1\d$\tws\log\DEUBA_range.log
set quelle=\\winsftp\d$\home\deuba\prod
set ziel=\\winsftp\d$\home\deuba\prod\Tranche


rem for /f %%i in ('dir /b %quelle%\dvag_range13*.gpg') do (call :entschluesseln %%i)
rem for /f %%i in ('dir /b %quelle%\TASK0007962713_final.*.gpg') do (call :entschluesseln %%i)
rem for /f %%i in ('dir /b %quelle%\pdf_expost.zip.gpg') do (call :entschluesseln %%i)
rem for /f %%i in ('dir /b %quelle%\pdf_expost_Rest.zip.gpg') do (call :entschluesseln %%i)
rem for /f %%i in ('dir /b %quelle%\expost*.csv.gpg') do (call :entschluesseln %%i)
for /f %%i in ('dir /b %quelle%\DVAG_Tranche*.gpg') do (call :entschluesseln %%i)
goto :EOF

:entschluesseln
set datei=%1
set zdatei=%~n1
echo %Date% %Time%: Entschluessele DEUBA_range Datei %datei% nach %ziel%
echo %Date% %Time%: Entschluessele DEUBA_range Datei %datei% nach %ziel%>>%log%
if exist %quelle% (
  if not exist %ziel% md %ziel%
  d:\tws\tools\pgp\gpg --keyring d:\tws\tools\pgp\keys/pubring.gpg --secret-keyring d:\tws\tools\pgp\keys/secring.gpg --output %ziel%\%zdatei% --load-extension "d:\tws\tools\pgp\idea.dll" --passphrase "blablabla" --decrypt %quelle%\%datei%
  if not errorlevel 1 goto :weiter
  goto :fehler

  :weiter
  rem d:\tws\tools\7z.exe e %ziel%\%datei%>
  echo %Date% %Time%: DEUBA_range Datei %datei% Transfer beendet
  echo -------------------------------------------------------------------------------
  echo %Date% %Time%: DEUBA_range Datei %datei% Transfer beendet>>%log%
  echo ------------------------------------------------------------------------------->>%log%
  rem del /Q %quelle%\%datei%
) else (echo %Date% %Time%: DEUBA_range Dateien wurden nicht gefunden
  echo -------------------------------------------------------------------------------
  echo %Date% %Time%: DEUBA_range Dateien wurden nicht gefunden>>%log%
  echo ------------------------------------------------------------------------------->>%log%
)
goto :EOF

:fehler
  echo %Date% %Time%: DEUBA_range Datei %datei% Transfer fehlerhaft: ABBRUCH!

goto :eof