#!/bin/bash
#autor: ggi
#30.11.2021

dir=/srv/ladon-blob
echo "starting ladon ... working directory is " $dir
java -Dcom.sun.management.jmxremote -javaagent:/srv/lib/jmx_prometheus_javaagent-0.16.2-SNAPSHOT.jar=9080:/srv/lib/config.yml\
     -Dspring.config.import=optional:file:./azure-config.properties\
     -Dladon.home=./ladon_data-blob\
     -jar ladon.jar &
echo $! > ./service.pid
echo "Ladon PID " $!