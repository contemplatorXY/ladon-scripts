#!/bin/sh
# ladon-cluster deployment
# author ggi
# date 26.03.2021
index="5,7"
(IFS=',';
  for inc in $index;
   do
     echo  deploing "lad$inc";
     ./ladon-restart.sh "lad$inc";
     echo "finished";
   done
)
echo "Ladon-Cluster restarted on Ladon Server $1"  | mail -s "Ladon-Restart: restart on Ladon Server $1" gerhard.gaida@itopea.de