#ladon restart
# author ggi
# date 26.03.2021

serverip=$1

echo "RESTARTING ladon.service on ladon server $serverip  ..."
ssh -tt -i.ssh/id_rsa supervisor@$serverip << EOF
cd /srv/lib
./shutdown.sh
sudo systemctl restart ladon.service
./start-cas-exp-stand.sh
exit
EOF
echo "DONE $serverip"
